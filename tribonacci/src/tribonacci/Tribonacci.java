/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tribonacci;

/**
 *
 * @author MILO
 */
public class Tribonacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numeros = new int[]{0, 0, 1};
        tribonacci(numeros, 9);
        // TODO code application logic here
    }
    static void tribonacci(int[] numeros,int devolver) {
        int numero1=numeros[0];
        int numero2=numeros[1];
        int numero3=numeros[2];
        int numeroSiguiente = 0;
        int colocarNumeros = 0;
        int [] listaDevolver = new int[devolver];
        
        
        if (devolver == 0){
            System.out.print("0");
            return;
        }
        
        for (int i=0; i< devolver ; i++)
        {
            if(colocarNumeros >= 0 && colocarNumeros < numeros.length){
             listaDevolver[i] = numeros[i];
             colocarNumeros++;
            }else{
                numeroSiguiente = numero1 + numero2 + numero3;
                listaDevolver[i] = numeroSiguiente;
                numero1 = numero2;
                numero2 = numero3;
                numero3 = numeroSiguiente;
            }
        } 
        
        System.out.print("[");
        for (int i=0; i< listaDevolver.length ; i++)
        {
            if(i < (listaDevolver.length -1) )
            {
                System.out.print(listaDevolver[i]+",");

            } else {
                System.out.print(listaDevolver[i]);
            }
         } 
        
        System.out.print("]");
    }
}
